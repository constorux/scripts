Scripts
======

## About

Hi! This is a repository for the various scripts that I have written to help
me solve different problems. If you find anything useful, feel free to use 
it.

## Disclaimer
Many of these scripts are just bodges which I wrote for a special purpose, 
so they might not be well tested. Use with 
caution

If you find a bug I would be happy if you would let me know 