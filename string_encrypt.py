import sys

encrypt = False
if input("encrypt (otherwise decrypt) [y/n]: ") == "y" :
	encrypt = True
	
passphrase = input("pass/hash : ")
key1 = input("key       : ")

if len(passphrase) > len(key1):
	sys.exit("ERROR: passphrase longer than the key")

while len(passphrase) < len(key1):
	passphrase = passphrase + "-"
	


def get_checksum(strng):
	o_checksm = ""
	for letter in strng:
		o_checksm = o_checksm +  str(ord(letter)).zfill(3)
	return str(int(o_checksm) % 100).zfill(2)

def convert(bool_encrypt, phrase, key):
	
	o_result = ""
	int_char = 0
	while int_char < len(key):
		char_code = ord(phrase[int_char])
		if char_code < 33 or char_code > 126:
			sys.exit("ERROR: passphrase contains illegal character >" + 
			phrase[int_char] + "<")
		
		if bool_encrypt:
			char_code = char_code + ord(key[int_char])
			while char_code > 126:
				char_code = 33 + (char_code - (126 + 1))
		else:
			char_code = char_code - ord(key[int_char])
			while char_code < 33:
				char_code = 126 - ((33 - 1) - char_code)
		
		o_result = o_result + chr(char_code)
		# print(char_code)
		int_char += 1
	return o_result


# print("result    : " + convert(encrypt, convert(encrypt, passphrase, key1), key2))

result = convert(encrypt, passphrase, key1)
print("")	
print("result    : " + result) 
print("-----------")
print("chk input : " + get_checksum(passphrase))
print("chk key   : " + get_checksum(key1))
print("chk output: " + get_checksum(result))
print("")	
input("press [enter] to exit..")